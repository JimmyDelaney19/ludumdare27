﻿using UnityEngine;
using System.Collections;

public class AnimationMethods : MonoBehaviour 
{

	protected GameObject GetChild(string name)
	{
		//Find and Set each child object for animation
		foreach(Transform child1 in gameObject.transform)
		{
			if(child1.name == name)
			{
				return child1.gameObject;
			}
			else
			{
				foreach(Transform child2 in child1.transform)
				{
					if(child2.name == name)
						return child2.gameObject;
					else
					{
						foreach(Transform child3 in child2.transform)
						{
							if(child3.name == name)
								return child3.gameObject;
							else
							{
								foreach(Transform child4 in child3.transform)
								{
									if(child4.name == name)
										return child4.gameObject;
									else
									{
										foreach(Transform child5 in child4.transform)
										{
											if(child5.name == name)
												return child5.gameObject;
											else
											{
												foreach(Transform child6 in child5.transform)
												{
													if(child6.name == name)
														return child6.gameObject;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
}
