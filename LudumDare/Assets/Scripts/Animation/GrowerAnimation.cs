﻿using UnityEngine;
using System.Collections;

public class GrowerAnimation : AnimationMethods
{
	
	public GameObject eyes, tooth, tooth1,
	lleg11, lleg12, lleg13, lleg21, lleg22, lleg31, lleg32, lleg41, lleg42, lfin,
	rleg11, rleg12, rleg13, rleg21, rleg22, rleg31, rleg32, rleg41, rleg42, rfin;
	
	
	void Start ()
	{
		
		//Find and Set each child object
		eyes = GetChild("Eyes");
		tooth = GetChild("Tooth");
		tooth1 = GetChild("Tooth1");
		lleg11 = GetChild("L_Leg1_1");
		lleg12 = GetChild("L_Leg1_2");
		lleg13 = GetChild("L_Leg1_3");
		lleg21 = GetChild("L_Leg2_1");
		lleg22 = GetChild("L_Leg2_2");
		lleg31 = GetChild("L_Leg3_1");
		lleg32 = GetChild("L_Leg3_2");
		lleg41 = GetChild("L_Leg4_1");
		lleg42 = GetChild("L_Leg4_2");
		lfin = GetChild("L_Leg5");
		rleg11 = GetChild("R_Leg1_1");
		rleg12 = GetChild("R_Leg1_2");
		rleg13 = GetChild("R_Leg1_3");
		rleg21 = GetChild("R_Leg2_1");
		rleg22 = GetChild("R_Leg2_2");
		rleg31 = GetChild("R_Leg3_1");
		rleg32 = GetChild("R_Leg3_2");
		rleg41 = GetChild("R_Leg4_1");
		rleg42 = GetChild("R_Leg4_2");
		rfin = GetChild("R_Leg5");

		//Starts animations, which then loop
		StartCoroutine(PassiveAnims(Random.Range(1.0f, 3.0f)));
	}
	
//	void Update ()
//	{
//		
//	}
	
	
	IEnumerator PassiveAnims(float playtime)
	{
		#region Gorgi Animations
		
		if(GetChild("Gorgi") != null)
		{
			//leg rotation value
			int llegAngle = 20;
			int rlegAngle = -20;
			
			Hashtable lLegHashFwd = new Hashtable();
				lLegHashFwd.Add("islocal", true);
				lLegHashFwd.Add("y", llegAngle/2); //POSITIVE
				lLegHashFwd.Add("time", (playtime / 2));
				lLegHashFwd.Add("easetype", iTween.EaseType.linear);
			
			Hashtable lLegHashBck = new Hashtable();
				lLegHashBck.Add("islocal", true);
				lLegHashBck.Add("y", -llegAngle/2); //NEGATIVE
				lLegHashBck.Add("time", (playtime / 2));
				lLegHashBck.Add("easetype", iTween.EaseType.linear);
			
			Hashtable rLegHashFwd = new Hashtable();
				rLegHashFwd.Add("islocal", true);
				rLegHashFwd.Add("y", rlegAngle/2);	//POSITIVE
				rLegHashFwd.Add("time", (playtime / 2));
				rLegHashFwd.Add("easetype", iTween.EaseType.linear);
			
			Hashtable rLegHashBck = new Hashtable();
				rLegHashBck.Add("islocal", true);
				rLegHashBck.Add("y", -rlegAngle/2); //NEGATIVE
				rLegHashBck.Add("time", (playtime / 2));
				rLegHashBck.Add("easetype", iTween.EaseType.linear);
			
			
		 	//start leg animations
			iTween.RotateTo(lleg11, lLegHashFwd);
			iTween.RotateTo(lleg12, lLegHashFwd);
			iTween.RotateTo(lleg13, lLegHashFwd);
			
			iTween.RotateTo(lleg21, lLegHashBck);
			iTween.RotateTo(lleg22, lLegHashBck);
			
			iTween.RotateTo(lleg31, lLegHashFwd);
			iTween.RotateTo(lleg32, lLegHashFwd);
			
			iTween.RotateTo(lleg41, lLegHashBck);
			iTween.RotateTo(lleg42, lLegHashBck);
			
			iTween.RotateTo(rleg11, rLegHashFwd);
			iTween.RotateTo(rleg12, rLegHashFwd);
			iTween.RotateTo(rleg13, rLegHashFwd);
			
			iTween.RotateTo(rleg21, rLegHashBck);
			iTween.RotateTo(rleg22, rLegHashBck);
			
			iTween.RotateTo(rleg31, rLegHashFwd);
			iTween.RotateTo(rleg32, rLegHashFwd);
			
			iTween.RotateTo(rleg41, rLegHashBck);
			iTween.RotateTo(rleg42, rLegHashBck);
			
			//Add delay to hashtables before reverse animation
			lLegHashFwd.Add("delay", (playtime / 2));
			lLegHashBck.Add("delay", (playtime / 2));
			rLegHashFwd.Add("delay", (playtime / 2));
			rLegHashBck.Add("delay", (playtime / 2));
			
			
			//reverses the legs back to the starting position
			iTween.RotateTo(lleg11, lLegHashBck);
			iTween.RotateTo(lleg12, lLegHashBck);
			iTween.RotateTo(lleg13, lLegHashBck);
			
			iTween.RotateTo(lleg21, lLegHashFwd);
			iTween.RotateTo(lleg22, lLegHashFwd);
			
			iTween.RotateTo(lleg31, lLegHashBck);
			iTween.RotateTo(lleg32, lLegHashBck);
			
			iTween.RotateTo(lleg41, lLegHashFwd);
			iTween.RotateTo(lleg42, lLegHashFwd);
			
			iTween.RotateTo(rleg11, rLegHashBck);
			iTween.RotateTo(rleg12, rLegHashBck);
			iTween.RotateTo(rleg13, rLegHashBck);
			
			iTween.RotateTo(rleg21, rLegHashFwd);
			iTween.RotateTo(rleg22, rLegHashFwd);
			
			iTween.RotateTo(rleg31, rLegHashBck);
			iTween.RotateTo(rleg32, rLegHashBck);
			
			iTween.RotateTo(rleg41, rLegHashFwd);
			iTween.RotateTo(rleg42, rLegHashFwd);
			
		}
		
		#endregion
		
		float delay = playtime;
		yield return new WaitForSeconds(delay);
		
		StartCoroutine(PassiveAnims(Random.Range(0.25f, 0.5f)));
	}
}
