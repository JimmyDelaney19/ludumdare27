﻿using UnityEngine;
using System.Collections;

public class HugeAnimation : AnimationMethods 
{
	public GameObject lclaw, rclaw, rspike1, rspike2, lspike1, lspike2;
	
	void Start()
	{
		
		//Find and Set each child object
		lclaw = GetChild("L_Claw");
		rclaw = GetChild("R_Claw");
		rspike1 = GetChild("R_Spike1");
		rspike2 = GetChild("R_Spike2");
		lspike1 = GetChild("L_Spike1");
		lspike2 = GetChild("L_Spike2");
		
		//Starts animations, which then loop
		StartCoroutine(PassiveAnims(Random.Range(1.0f, 3.0f)));
	}

	
	
	IEnumerator PassiveAnims(float playtime)
	{
	#region JellyBean Animations
		
		if(GetChild("JellyBean") != null)
		{
			//claw rotate values
			int lclawAngle = 30;
			int rclawAngle = -30;
			
			//start claw animation
			iTween.RotateTo(lclaw, iTween.Hash("islocal", true, "y", lclawAngle, "time", (playtime / 2), "easetype", iTween.EaseType.easeInExpo));
			iTween.RotateTo(rclaw, iTween.Hash("islocal", true, "y", rclawAngle, "time", (playtime / 2), "easetype", iTween.EaseType.easeInExpo));
			
			
	//		//reverses the claws back to the starting position
			iTween.RotateTo(lclaw, iTween.Hash("islocal", true, "delay", (playtime / 2), "y", -lclawAngle, "time", (playtime / 2), "easetype", iTween.EaseType.easeOutExpo));
			iTween.RotateTo(rclaw, iTween.Hash("islocal", true, "delay", (playtime / 2), "y", -rclawAngle, "time", (playtime / 2), "easetype", iTween.EaseType.easeOutExpo));
			
			//start spike animations
			//These (delays) randomize the spike shakes
			//start off with a random delay within the "playtime" limits
			float delay = Random.Range(0, playtime);
			yield return new WaitForSeconds(delay);
			
			
			lspike1.transform.localRotation = Quaternion.Euler(Vector3.zero);
			rspike1.transform.localRotation = Quaternion.Euler(Vector3.zero);
			
			iTween.RotateFrom(lspike1, iTween.Hash("islocal", true, "y", 10, "time", Random.Range(0.5f, 5), "easetype", iTween.EaseType.easeOutElastic));
			iTween.RotateFrom(rspike1, iTween.Hash("islocal", true, "y", -10, "time", Random.Range(0.5f, 5), "easetype", iTween.EaseType.easeOutElastic));
			
			delay = (-1 * (delay - playtime));
			yield return new WaitForSeconds(delay);
			
			lspike2.transform.localRotation = Quaternion.Euler(Vector3.zero);
			rspike2.transform.localRotation = Quaternion.Euler(Vector3.zero);
			
			iTween.RotateFrom(lspike2, iTween.Hash("islocal", true, "y", 10, "time", Random.Range(0.5f, 5), "easetype", iTween.EaseType.easeOutElastic));
			iTween.RotateFrom(rspike2, iTween.Hash("islocal", true, "y", -10, "time", Random.Range(0.5f, 5), "easetype", iTween.EaseType.easeOutElastic));
		}
		
		#endregion
		
		StartCoroutine(PassiveAnims(Random.Range(0.5f, 1.0f)));
	}
}
