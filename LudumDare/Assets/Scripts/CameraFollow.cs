﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	public Vector3 targetPosition;
	public GameObject player;
	public float iOffset;
	public float OrthographicOffsetFactor = 10;
	public float moveSpeed = 10;
	public float moveTimer = 0f;
	// Use this for initialization
	void Start () {
	//player = GameObject.Find("Player");
	iOffset = transform.position.z - Globals.instance.player.transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		FollowPlayer();
	}
	
	void FollowPlayer()
	{
		player = Globals.instance.player;
		
		if(player)
		{
			//If camera is perspective
			if(!Camera.main.isOrthoGraphic)
			{
				iOffset = -40 - player.transform.localScale.x*10 + 10;
			}
			
			//If orthographic, adjust the orthographic field of view
			else if(Camera.main.isOrthoGraphic)
			{
				//Size is equal to the player's size times the offset factor

				if(player.transform.localScale.x * OrthographicOffsetFactor < 10)
				{
					Camera.main.orthographicSize = 10;
				}
				else
				{
					Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize,player.transform.localScale.x * OrthographicOffsetFactor,1*Time.deltaTime);
				}
			}
			
			
			targetPosition = new Vector3(player.transform.position.x + player.rigidbody.velocity.x * 1.5f/15 * player.transform.localScale.x,
										 player.transform.position.y + player.rigidbody.velocity.y * 1.5f/15 * player.transform.localScale.y, iOffset);
			
			transform.position = Vector3.Lerp(transform.position,targetPosition,Time.deltaTime*10000000);
		}
		
	}
}
