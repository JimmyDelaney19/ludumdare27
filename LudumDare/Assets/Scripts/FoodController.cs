using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {
	
	public GameObject Food;
	public GameObject currentFood;
	public float moveSpeed = 10;
	public float moveTimer = 0f;
	public float pullForce = 500000;
	public Vector3 tarPos;
	public ParticleSystem deathParticles;
	public bool animate;
	
	
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		moveTimer-=Time.deltaTime;
			
		if(moveTimer<0)
		{		tarPos = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),0).normalized;
				moveTimer = 0.3f;
		}
		moveFood();
	}
	
	public void moveFood()
	{
		rigidbody.AddForce(tarPos * Time.deltaTime * moveSpeed);
	}
	
	public void AnimateOffset(Vector2 pixelOffset)
	{
		 GameObject.Find("Text").transform.position = pixelOffset;
	}
	
	void OnTriggerEnter(Collider other)
	{
		//if the thing the food hits is an enemy or player
		if(other.GetComponent<Controller>() && !other.GetComponent<FoodLayerController>())
		{
			if(gameObject.name == "MenuFood(Clone)")
			{
				other.transform.localScale += transform.localScale*5;
			}
			else if(other.GetComponent<PlayerController>())
			{
				//other.transform.localScale += (transform.localScale/20)/(Mathf.Pow(transform.localScale.x,0.25f));
			
				other.GetComponent<PlayerController>().TweenScale(other.GetComponent<PlayerController>().currentTo, other.GetComponent<PlayerController>().currentTo + (transform.localScale.x/40)/(Mathf.Pow(transform.localScale.x,0.25f)));
			}
			else
			{
				other.transform.localScale += (transform.localScale/20)/(Mathf.Pow(transform.localScale.x,0.50f));
			}
			if(other.GetComponent<PlayerController>())
			{
				Globals.instance.points +=10;
				Globals.instance.pointLabel.GetComponent<UILabel>().text = "10";
				TweenPosition.Begin(Globals.instance.pointLabel,0,new Vector3(0,0,0));

				GameManager.instance.TweenPoints();

				
				GameObject.Find("GUI").GetComponent<GUIScript>().UpdateScore();
				SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.FoodEat);
			}
			Instantiate(deathParticles,transform.position,transform.rotation);
			Destroy(gameObject);
			
			
		}
		
	}
	void OnTriggerStay(Collider other)
	{
		if(other.gameObject.name == "GravityUpgradeEffect")
		{
			rigidbody.AddForce((other.transform.position-transform.position).normalized*Time.deltaTime*pullForce);
		}
	}
}
