using UnityEngine;
using System.Collections;

public class FoodLayerController : Controller {
	
	public GameObject closestEnemy;
	public float moveSpeed = 100000;
	public GameObject particles;
	public float foodDropTimer = 1.5f;
	public float growthRate = 100;
	public ParticleSystem playerDeath;
	public GameObject Food;
	public GameObject currentFood;
	// Use this for initialization
	void Start () {
		
		GetClosestEnemy();
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.up = Vector3.Lerp(transform.up, -rigidbody.velocity, Time.deltaTime);
		if(!closestEnemy)
		{
			GetClosestEnemy();
		}
		if(Globals.instance.passiveWorld)
		{
			foodDropTimer-=Time.deltaTime;
			moveSpeed = 400;
			if(foodDropTimer < 0)
			{
				DropFood();
				foodDropTimer =2;
			}
			MoveAway();
		}
		else
		{
			moveSpeed = 500;
			AttackEnemies();
		}
	}
	
	void DropFood()
	{
		audio.PlayOneShot(SM.instance.FoodDrop); //plays on self in 3d space
		currentFood = Instantiate(Food, transform.position, Quaternion.Euler(0,0,0)) as GameObject;
		currentFood.transform.parent = Globals.instance.FoodObjects.transform;
		//currentFood.transform.localScale =transform.localScale/20;
	}
	void AttackEnemies()
	{
		if(!closestEnemy)
		{
			GetClosestEnemy();
		
		}
		rigidbody.AddForce((closestEnemy.transform.position - transform.position).normalized * Time.deltaTime * moveSpeed);	
	}
	
	void MoveAway()
	{
		if(closestEnemy)
		{
			rigidbody.AddForce((transform.position - closestEnemy.transform.position ).normalized * Time.deltaTime * moveSpeed);
		}
	}
	
	void GetClosestEnemy()
	{
		
		
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				
				if(!closestEnemy)
				{
					closestEnemy = go;
				}
				else
				{
					//IF go is closer than current enemy, && go is smaller, && this is larger, && go is not a Killer
					if(Vector3.Distance(go.transform.position,transform.position) < Vector3.Distance(closestEnemy.transform.position,transform.position)&& go.transform.localScale.x < transform.localScale.x && !go.GetComponent<KillerController>())
					{
						closestEnemy = go;
					}
				}
				if(closestEnemy == gameObject)
				{
					closestEnemy = null;
				}
			}
		
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Controller>())
		{
			if(other.GetComponent<PlayerController>())
			{
				if(Globals.instance.passiveWorld)//if its a passive world, then bigger eats smaller
				{
				
					if(other.gameObject.transform.localScale.x > transform.localScale.x)
					{
						//When food just gets eaten by player
						SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.FoodEat);
						
						other.transform.localScale += transform.localScale/10;
						Globals.instance.points +=transform.localScale.x*50f;
						Globals.instance.pointLabel.GetComponent<UILabel>().text = (transform.localScale.x*50f).ToString("00");
						TweenPosition.Begin(Globals.instance.pointLabel,0,new Vector3(0,0,0));
						GameManager.instance.TweenPoints();
						GameObject.Find("GUI").GetComponent<GUIScript>().UpdateScore();
						
						Destroy(gameObject);
						
					}
					else
					{
						transform.localScale += other.transform.localScale/10;
						particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
						SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.PlayerDeath);
						
						if(Globals.instance.player)
						{
						//particle.transform
						//particles.transform.localScale = Globals.instance.player.transform.localScale;
						}
						Destroy(other.gameObject);
					}
					
				}
				else //if its aggressive it doesn't matter how big you are
				{
					transform.localScale += other.transform.localScale/10;
					particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
					SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.PlayerDeath);
					
					if(Globals.instance.player)
					//particles.transform.localScale = Globals.instance.player.transform.localScale;
					Destroy(other.gameObject);
				}
				
			}
			else
			{
				if(other.gameObject.transform.localScale.x > transform.localScale.x && !other.GetComponent<KillerController>())
				{
					//other.transform.localScale += transform.localScale/10;
					//Destroy(gameObject);
					
				}
				else if(other.GetComponent<KillerController>())
				{
					Destroy(gameObject);
				}
				else
				{
					float tempFloat = (transform.localScale.x/growthRate)/(Mathf.Pow(transform.localScale.x,0.25f));
					
					transform.localScale += new Vector3(tempFloat, tempFloat, tempFloat);
					Destroy(other.gameObject);
				}
			}
		} 
	}
}
