﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {
	
	public GameObject pointLabel;
	// Use this for initialization
	void Start () {
	
		//DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void UpdateScore()
	{
		pointLabel.GetComponent<UILabel>().text = "Points: " + Globals.instance.points.ToString("00");
		if(Globals.instance.freePlay && Globals.instance.points > 5000)
		{
			PlayerPrefs.SetInt("DayNightMode", 1);
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Free Play";

		}
		if(Globals.instance.freePlay || Globals.instance.nightMode)
		{
			return;
		}

		if(GameObject.Find("GM").GetComponent<GameManager>().level==1 && Globals.instance.points >= Globals.instance.Level1Points)
		{
			GameObject.Find("GM").GetComponent<GameManager>().level++;
			Globals.instance.player.GetComponent<SphereCollider>().enabled = false;
				//collider.enabled =false;
			iTween.AudioTo(GameObject.Find("Music"),0,1,2);
			ShowLevel ();
			
		}
		else if(GameObject.Find("GM").GetComponent<GameManager>().level==3 && Globals.instance.points >= Globals.instance.Level3Points)
		{
			GameObject.Find("GM").GetComponent<GameManager>().level++;
			Globals.instance.player.GetComponent<SphereCollider>().enabled = false;
			iTween.AudioTo(GameObject.Find("Music"),0,1,2);
			ShowLevel ();
			
		}
		else if(GameObject.Find("GM").GetComponent<GameManager>().level==4 && Globals.instance.points >= Globals.instance.Level4Points)
		{
			GameObject.Find("GM").GetComponent<GameManager>().level++;
			Globals.instance.player.GetComponent<SphereCollider>().enabled = false;
			iTween.AudioTo(GameObject.Find("Music"),0,1,2);
			ShowLevel ();
			
		}
	}
	
	public void ShowLevel()
	{
		TweenPosition.Begin(Globals.instance.ObjLabel,0.1f,new Vector3(0,111,0));
				TweenScale.Begin(Globals.instance.ObjLabel,0.1f,new Vector3(70f,70f,0));
				//GameObject.Find("GM").GetComponent<GameManager>().survivalTimer  = 60;
				GameManager GM = GameObject.Find("GM").GetComponent<GameManager>();
				if(GM.level == 2)
				{
					GM.levelObjective = "Survive for 60 Seconds!";
					Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + GM.level + "\n" + GM.levelObjective;
					
				}
				else if(GM.level == 3)
				{
					GM.levelObjective = "Get " + Globals.instance.Level3Points + " Points!";
					Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + GM.level + "\n" + GM.levelObjective;
					
				}
				else if(GM.level == 4)
				{
					GM.levelObjective = "Get " + Globals.instance.Level4Points + " Points!";
					Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + GM.level + "\n" + GM.levelObjective;
					
				}
				Globals.instance.startButton.SetActive(true);
	}
	
	
	
}
