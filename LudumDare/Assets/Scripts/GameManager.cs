﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	public PlayerController _PlayerController;
	public GameObject Food;
	public GameObject Grower;
	public GameObject Layer;
	public GameObject SpeedUpgrade;
	public GameObject Huge;
	public GameObject Killer;
	public GameObject Tweener;
	public GameObject GravityUpgrade;
	public GameObject playerPrefab;
	public GameObject panel;
	public GameObject titlePanel;
	public string levelObjective;
	public int NumberOfFood;
	public int NumberOfGrowers;
	public int NumberOfLayers;
	public int NumberOfHuges;
	public int NumberOfKillers;
	public int NumberOfTweeners;
	public int NumberOfSpeedUpgrades;
	public int NumberOfGravityUpgrades;
	public float assignTextures = 0.2f;
	public float variance = 0.5f;
	public float growerVariance = 0.05f;
	public float currentRandom;
	public int level = 1;
	public float survivalTimer =60;
	public bool level2Started;
	public float boundaryCheckTimer = 1.0f;
	public bool gameStarted;
	//[HideInInspector]
	public float boundaryLength = 150;

	public static GameManager instance;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		
	Time.timeScale = 1.0f;

		if(PlayerPrefs.GetInt("DayNightMode") == 1)
		{

		Globals.instance.nightButton.SetActive(true);
		Globals.instance.nightButton.transform.GetChild(0).GetComponent<UILabel>().text = "Night Mode";
		Globals.instance.nightButton.transform.collider.enabled = true;
		}
	}
	
	void Update()
	{
		
		if(level==2&& level2Started && Globals.instance.player)
		{
			survivalTimer -= Time.deltaTime;
			GameObject.Find("TimeLabel").GetComponent<UILabel>().text = survivalTimer.ToString("0.00");
			if(survivalTimer <=0)
			{
				level = 3;
				Globals.instance.player.GetComponent<SphereCollider>().enabled = false;
				iTween.AudioTo(GameObject.Find("Music"),0,1,2);
				GameObject.Find("TimeLabel").GetComponent<UILabel>().text = "";
				GameObject.Find("GUI").GetComponent<GUIScript>().ShowLevel();
			}
		}
		assignTextures -= Time.deltaTime;
		if(assignTextures<0)
		{
			assignTextures = 0.2f;
			AssignMaterials();
		}
		
		boundaryCheckTimer -= Time.deltaTime;
		if(boundaryCheckTimer < 0)
		{
			boundaryCheckTimer = 1.0f;
			if(Globals.instance.player)
			{
				CheckBoundaries();
			}
		}
		
	}
	
	public void CheckBoundaries()
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("BG"))
		{
			if(Globals.instance.player.transform.position.x > go.transform.position.x + 2000)
			{
				go.transform.position += new Vector3(3000,0,0);
			}
			else if(Globals.instance.player.transform.position.x < go.transform.position.x - 2000)
			{
				go.transform.position += new Vector3(-3000,0,0);
			}
			
			if(Globals.instance.player.transform.position.y > go.transform.position.y + 2000)
			{
				go.transform.position += new Vector3(0,3000,0);
			}
			else if(Globals.instance.player.transform.position.y < go.transform.position.y - 2000)
			{
				go.transform.position += new Vector3(0,-3000,0);
			}
		}

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Caustic"))
		{
			if(Globals.instance.player.transform.position.x > go.transform.position.x + 150f)
			{
				go.transform.position += new Vector3(307.2f,0,0);
			}
			else if(Globals.instance.player.transform.position.x < go.transform.position.x - 150f)
			{
				go.transform.position += new Vector3(-307.2f,0,0);
			}
			
			if(Globals.instance.player.transform.position.y > go.transform.position.y + 150f)
			{
				go.transform.position += new Vector3(0,307.2f,0);
			}
			else if(Globals.instance.player.transform.position.y < go.transform.position.y - 150f)
			{
				go.transform.position += new Vector3(0,-307.2f,0);
			}
		}

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			if(Globals.instance.player.transform.position.x > go.transform.position.x + boundaryLength/2)
			{
				go.transform.position += new Vector3(boundaryLength,0,0);
			}
			else if(Globals.instance.player.transform.position.x < go.transform.position.x - boundaryLength/2)
			{
				go.transform.position += new Vector3(-boundaryLength,0,0);
			}
			
			if(Globals.instance.player.transform.position.y > go.transform.position.y + boundaryLength/2)
			{
				go.transform.position += new Vector3(0,boundaryLength,0);
			}
			else if(Globals.instance.player.transform.position.y < go.transform.position.y - boundaryLength/2)
			{
				go.transform.position += new Vector3(0,-boundaryLength,0);
			}
		}
		
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Food"))
		{
			if(Globals.instance.player.transform.position.x > go.transform.position.x + boundaryLength/2)
			{
				go.transform.position += new Vector3(boundaryLength,0,0);
			}
			else if(Globals.instance.player.transform.position.x < go.transform.position.x - boundaryLength/2)
			{
				go.transform.position += new Vector3(-boundaryLength,0,0);
			}
			
			if(Globals.instance.player.transform.position.y > go.transform.position.y + boundaryLength/2)
			{
				go.transform.position += new Vector3(0,boundaryLength,0);
			}
			else if(Globals.instance.player.transform.position.y < go.transform.position.y - boundaryLength/2)
			{
				go.transform.position += new Vector3(0,-boundaryLength,0);
			}
		}
	}

	public void LoadNightMode()
	{
		panel.SetActive(true);
		Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Day Night Mode";
		Globals.instance.nightMode = true;
		Globals.instance.freePlay = false;
		Globals.instance.playPanel.SetActive(false);
		Globals.instance.titlePanel.SetActive(false);
		titlePanel.SetActive(false);
		iTween.AudioTo(GameObject.Find("Music"),0,1,2);
	}

	public void LoadDayMode()
	{
		panel.SetActive(true);
		Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Free Play Mode";
		Globals.instance.freePlay = true;
		Globals.instance.playPanel.SetActive(false);
		Globals.instance.titlePanel.SetActive(false);
		titlePanel.SetActive(false);
		iTween.AudioTo(GameObject.Find("Music"),0,1,2);
	}

	public void LoadFirstLevelMenu()
	{
		
		panel.SetActive(true);
		Globals.instance.freePlay = false;
		Globals.instance.playPanel.SetActive(false);
		Globals.instance.titlePanel.SetActive(false);
		titlePanel.SetActive(false);
		iTween.AudioTo(GameObject.Find("Music"),0,1,2);

		
	}
	
	public Vector3 getSpawnPosition()
	{
			Vector3 randomPos;
			float randomx;
			float randomy;
			int randomChoice = Random.Range(0,2);
			if(randomChoice ==0)
			{
				randomx = Random.Range(-boundaryLength/2,-10)+ Globals.instance.player.transform.position.x;
			}
			else
			{
				randomx = Random.Range(10,boundaryLength/2)+ Globals.instance.player.transform.position.x;
			}
		
			randomChoice = Random.Range(0,2);
		
		
			if(randomChoice ==0)
			{
				randomy = Random.Range(-boundaryLength/2,-10)+ Globals.instance.player.transform.position.y;
			}
			else
			{
				randomy = Random.Range(10,boundaryLength/2)+ Globals.instance.player.transform.position.y;
			}
			
			randomPos = new Vector3(randomx, randomy, 0.1f);
			
			return randomPos;
	}
	
	public Vector3 getLaterSpawnPosition()
	{
			Vector3 randomPos;
			float randomx;
			float randomy;
			int randomChoice = Random.Range(0,2);
			if(randomChoice ==0)
			{
				randomx = Random.Range(-boundaryLength/2,-50)+ Globals.instance.player.transform.position.x;
			}
			else
			{
				randomx = Random.Range(50,boundaryLength/2)+ Globals.instance.player.transform.position.x;
			}
		
			randomChoice = Random.Range(0,2);
		
		
			if(randomChoice ==0)
			{
				randomy = Random.Range(-boundaryLength/2,-50)+ Globals.instance.player.transform.position.y;
			}
			else
			{
				randomy = Random.Range(50,boundaryLength/2)+ Globals.instance.player.transform.position.y;
			}
			
			randomPos = new Vector3(randomx, randomy, 0.1f);
			
			return randomPos;
	}
	public void InitialSpawn()
	{
		gameStarted = true;
		Globals.instance.player.GetComponent<SphereCollider>().enabled = true;
		RenderSettings.fog = false;
		iTween.AudioTo(GameObject.Find("Music"),0.6f,1,2);
		iTween.AudioTo(GameObject.Find("SoundFX"),0.4f,1,2);

		if(Globals.instance.freePlay)
		{
			if(PlayerPrefs.GetInt("UnlockDayNight")== 0)
			{
				levelObjective = "Free Play 5000 points to Unlock\n Day Night Mode";
				Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Free Play 5000 points\n to Unlock Day Night Mode";

			}
			else
			{
				levelObjective = "Free Play";
				Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Free Play";
			}
			
			NumberOfFood = 450;
			NumberOfGrowers = 50;
			NumberOfLayers = 150;
			NumberOfSpeedUpgrades = 10;
			NumberOfGravityUpgrades = 5;
			NumberOfHuges = 10;
			NumberOfKillers = 10;
			NumberOfTweeners = 0;
		}
		if(Globals.instance.nightMode)
		{

			levelObjective = "Day Night Mode";
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Day Night Mode";
						
			NumberOfFood = 450;
			NumberOfGrowers = 50;
			NumberOfLayers = 150;
			NumberOfSpeedUpgrades = 10;
			NumberOfGravityUpgrades = 5;
			NumberOfHuges = 10;
			NumberOfKillers = 10;
			NumberOfTweeners = 0;
		}
		else if(level == 1)
		{
			levelObjective = "Get " + Globals.instance.Level1Points + " Points!";
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + level + "\n" + levelObjective;
//			NumberOfFood = 1000;
//			NumberOfGrowers = 20;
//			NumberOfLayers = 10;
//			NumberOfSpeedUpgrades = 0;
//			NumberOfGravityUpgrades = 0;
//			NumberOfHuges = 0;
//			NumberOfKillers = 0;
//			NumberOfTweeners = 40;
			
			NumberOfFood = 450;
			NumberOfGrowers = 25;
			NumberOfLayers = 5;
			NumberOfSpeedUpgrades = 0;
			NumberOfGravityUpgrades = 40;
			NumberOfHuges = 6;
			NumberOfKillers = 0;
			NumberOfTweeners = 5;
					
		}
		else if(level == 2)
		{
			level2Started = true;
			levelObjective = "Survive for 60 Seconds!";
			survivalTimer = 60;
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + level + "\n" + levelObjective;
			NumberOfFood = 50;
			NumberOfGrowers = 50;
			NumberOfLayers = 60;
			NumberOfSpeedUpgrades = 20;
			NumberOfGravityUpgrades = 0;
			NumberOfHuges = 0;
			NumberOfKillers = 15;
			NumberOfTweeners = 0;
					
		}
		else if(level == 3)
		{
				levelObjective = "Get " + Globals.instance.Level3Points + " Points!";
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + level + "\n" + levelObjective;
			NumberOfFood = 1000;
			NumberOfGrowers = 50;
			NumberOfLayers = 20;
			NumberOfSpeedUpgrades = 10;
			NumberOfGravityUpgrades = 0;
			NumberOfHuges = 10;
			NumberOfKillers = 5;
			NumberOfTweeners = 0;
					
		}
		else if(level == 4)
		{
				levelObjective = "Get " + Globals.instance.Level4Points + " Points!";
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + level + "\n" + levelObjective;
			NumberOfFood = 300;
			NumberOfGrowers = 150;
			NumberOfLayers = 100;
			NumberOfSpeedUpgrades = 5;
			NumberOfGravityUpgrades = 0;
			NumberOfHuges = 10;
			NumberOfKillers = 10;
			NumberOfTweeners = 0;
					
		}
		else if(level == 5)
		{
			levelObjective = "Get so big the game crashes";
			Globals.instance.ObjLabel.GetComponent<UILabel>().text = "Level " + level + "\n" + levelObjective;
			NumberOfFood = 1250;
			NumberOfGrowers = 100;
			NumberOfLayers = 800;
			NumberOfSpeedUpgrades = 5;
			NumberOfGravityUpgrades = 0;
			NumberOfHuges = 10;
			NumberOfKillers = 5;
			NumberOfTweeners = 0;
					
		}
		
	
			for(int i=0; i<NumberOfFood; i++)
			{
			
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentFood = Instantiate(Food,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentFood.transform.parent = Globals.instance.FoodObjects.transform;
			}
			
			for(int i=0; i<NumberOfGrowers; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentEnemy = Instantiate(Grower,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentEnemy.transform.parent = Globals.instance.Enemies.transform;
				currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance, Globals.instance.player.transform.localScale.x + growerVariance);
				currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
			}
			
			for(int i=0; i<NumberOfLayers; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentEnemy = Instantiate(Layer,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentEnemy.transform.parent = Globals.instance.Enemies.transform;
				currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance, Globals.instance.player.transform.localScale.x + growerVariance);
				currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
			}
			for(int i=0; i<NumberOfSpeedUpgrades; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentFood = Instantiate(SpeedUpgrade,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentFood.transform.parent = Globals.instance.FoodObjects.transform;
			}
			for(int i=0; i<NumberOfGravityUpgrades; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentFood = Instantiate(GravityUpgrade,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentFood.transform.parent = Globals.instance.FoodObjects.transform;
			}
			
			for(int i=0; i<NumberOfHuges; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentEnemy = Instantiate(Huge,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentEnemy.transform.parent = Globals.instance.Enemies.transform;
				currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance + 0.5f, Globals.instance.player.transform.localScale.x + growerVariance+ 0.5f);
				currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
			}
		
			for(int i=0; i<NumberOfKillers; i++)
			{
				//setting the parent so when we play the game, food doesn't show up everywhere
				GameObject currentEnemy = Instantiate(Killer,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
				currentEnemy.transform.parent = Globals.instance.Enemies.transform;
				//currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance + 2, Globals.instance.player.transform.localScale.x + growerVariance+ 2);
				//currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
			}
		
//				for(int i=0; i<NumberOfTweeners; i++)
//			{
//				//setting the parent so when we play the game, food doesn't show up everywhere
//				GameObject currentEnemy = Instantiate(Tweener,getSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
//				currentEnemy.transform.parent = Globals.instance.Enemies.transform;
//				//currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance + 2, Globals.instance.player.transform.localScale.x + growerVariance+ 2);
//				//currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
//			}
		
	}
	public void RestartLevel()
	{
		Globals.instance.inMenus = false;

		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			Destroy(go);
		}
		
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Food"))
		{
			Destroy(go);
		}
		
		if(GameObject.Find("Player"))
		{
			Destroy(GameObject.Find("Player"));
		}
		
		if(GameObject.Find("Player(Clone)"))
		{
			Destroy(GameObject.Find("Player(Clone)"));
		}
		
		Globals.instance.points = 0;
		
		GameObject currentPlayer = Instantiate(playerPrefab,transform.position,transform.rotation) as GameObject;
		GameObject.Find("Main Camera").transform.position = new Vector3(0,0,-50);
		GameObject.Find("Main Camera").GetComponent<CameraFollow>().player = currentPlayer;
		Globals.instance.player = currentPlayer;
		
		//_PlayerController = GameObject.Find("Player(Clone)").GetComponent<PlayerController>();
		
		Globals.instance.player.GetComponent<PlayerController>().invincibleUsed = false;
		Globals.instance.player.GetComponent<PlayerController>().upgradedSpeed = false;
		Globals.instance.player.GetComponent<PlayerController>().upgradedGravity = false;
		Globals.instance.player.transform.FindChild("GravityUpgradeEffect").collider.enabled = false;
		Globals.instance.player.GetComponentInChildren<ParticleSystem>().Stop();
		Globals.instance.player.GetComponentInChildren<ParticleSystem>().Clear();
		Globals.instance.player.GetComponent<PlayerController>().moveSpeed = 1500;
		Globals.instance.passiveWorld = true;
		
		//foreach(GameObject go in GameObject.FindGameObjectsWithTag("BG"))
		//{
		//		go.renderer.material = Resources.Load("Materials/BG_01") as Material;
		//}
		SM.instance.Music.audio.Stop ();
		SM.instance.Music.audio.Play ();
		Globals.instance.turnTimer = 10;
		InitialSpawn();
		GameObject.Find("GUI").GetComponent<GUIScript>().UpdateScore();

		Time.timeScale = 0;
		iTween.MoveTo(Globals.instance.ObjLabel, iTween.Hash(
			"position"			,	Globals.instance.objTweenPosition,
			"ignoretimescale"	,	true,
			"islocal"			,	true,
			"time"				,	1,
			"easetype"			,	iTween.EaseType.easeOutQuad,
			"oncomplete"		,	"ResumeGame",
			"oncompletetarget"	,	gameObject));


		TweenScale.Begin(Globals.instance.ObjLabel,2.0f,new Vector3(40f,40f,0));
		Globals.instance.startButton.SetActive(false);

		Globals.instance.player.GetComponent<PlayerController>().TurnOffSpeedDelayed();
		
	}

	public void TweenPoints()
	{
		Globals.instance.pointLabel.SetActive(true);

		iTween.MoveTo(Globals.instance.pointLabel, iTween.Hash(
			"position"			,	Globals.instance.pointTweenPosition,
			"ignoretimescale"	,	true,
			"islocal"			,	true,
			"time"				,	1,
			"easetype"			,	iTween.EaseType.easeOutQuad,
			"oncomplete"		,	"TurnOffLabel",
			"oncompletetarget"	,	gameObject));
	}

	void TurnOffLabel()
	{
		Globals.instance.pointLabel.SetActive(false);
	}

	void ResumeGame()
	{
		Time.timeScale = 1;
	}

	public void SpawnFood()
	{
		for(int i=0; i<NumberOfFood/8.0f; i++)
		{
			//setting the parent so when we play the game, food doesn't show up everywhere
			GameObject currentFood = Instantiate(Food,getLaterSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
			currentFood.transform.parent = Globals.instance.FoodObjects.transform;
		}
		for(int i=0; i<NumberOfSpeedUpgrades/5.0f; i++)
		{
			//setting the parent so when we play the game, food doesn't show up everywhere
			GameObject currentFood = Instantiate(SpeedUpgrade,getLaterSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
			currentFood.transform.parent = Globals.instance.FoodObjects.transform;
		}
		for(int i=0; i<NumberOfGravityUpgrades/3.0f; i++)
		{
			//setting the parent so when we play the game, food doesn't show up everywhere
			GameObject currentFood = Instantiate(GravityUpgrade,getLaterSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
			currentFood.transform.parent = Globals.instance.FoodObjects.transform;
		}
		
	}
	public void SpawnEnemys()
	{
		if(!gameStarted)
		{
			return;
		}
		for(int i=0; i<NumberOfGrowers/4; i++)
			{
				
					GameObject currentEnemy = Instantiate(Grower, getLaterSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
					currentEnemy.transform.parent = Globals.instance.Enemies.transform;
					currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance, Globals.instance.player.transform.localScale.x + growerVariance);
					currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
				
			}
		
		for(int i=0; i<NumberOfLayers/4; i++)
		{
			
					//setting the parent so when we play the game, food doesn't show up everywhere
					GameObject currentEnemy = Instantiate(Layer,getLaterSpawnPosition(), Quaternion.Euler(0,0,0)) as GameObject;
					currentEnemy.transform.parent = Globals.instance.Enemies.transform;
					currentRandom = Random.Range(Globals.instance.player.transform.localScale.x - growerVariance, Globals.instance.player.transform.localScale.x + growerVariance);
					currentEnemy.transform.localScale = new Vector3(currentRandom,currentRandom,currentRandom);
				
		}
		
		
	}
	public void AssignMaterials()
	{
		foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
		{
			if(Globals.instance.player)
			{
				
				
				
				//GROWER
				if(!go.GetComponent<HugeController>() && !go.GetComponent<FoodLayerController>() && !go.GetComponent<KillerController>())
				{
					if(go.transform.localScale.x < Globals.instance.player.transform.localScale.x && Globals.instance.passiveWorld )
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.GreenHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.greenColor;
					}
					else
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.RedHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.redColor;
					}
				}
				//HUGE
				else if(go.GetComponent<HugeController>())
				{
					if(go.transform.localScale.x < Globals.instance.player.transform.localScale.x && Globals.instance.passiveWorld )
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.GreenHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.greenColor;
					}
					else
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.RedHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.yellowColor;
					}
					
				}
				//LAYER
				else if(go.GetComponent<FoodLayerController>())
				{
					if(go.transform.localScale.x < Globals.instance.player.transform.localScale.x && Globals.instance.passiveWorld )
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.GreenHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.greenColor;
					}
					else
					{
						go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.RedHalo;
//						go.GetComponentInChildren<Light>().color = Globals.instance.orangeColor;
					}
					
				}
				//KILLER
				else if(go.GetComponent<KillerController>())
				{
					
					go.transform.FindChild("Circle").GetComponent<MeshRenderer>().material =  Globals.instance.RedHalo;
//					go.GetComponentInChildren<Light>().color = Globals.instance.purpleColor;
				}
				
				
			}	
		}
	}
}
