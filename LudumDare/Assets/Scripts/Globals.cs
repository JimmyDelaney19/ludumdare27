using UnityEngine;
using System.Collections;

public class Globals : MonoBehaviour {

	//SINGLETON INSTANCE
	public static Globals instance;

	public GameObject ObjLabel;
	public GameObject startButton;
	public GameObject guiPointLabel;
	public GameObject player;
	public GameObject FoodObjects;
	public GameObject Enemies;
	public GameObject Food;
	public GameObject deathLabel;
	public GameObject pointLabel;
	public GameObject menuFood;
	public GameObject titlePanel, playPanel, titleMenuPanel;
	public GameObject [] BG1, BG2, BG3, BG4;
	public GameObject nightButton;

	public float moveTimer = 3.0f;
	public FoodController FC;
	public Material red;
	public Material green;
	public Material blue;
	public Material yellow;
	public Material orange;
	public Material purple;
	public Material RedHalo;
	public Material GreenHalo;
	public Color redColor;
	public Color blueColor;
	public Color greenColor;
	public Color yellowColor;
	public Color orangeColor;
	public Color purpleColor;
	public bool freePlay;
	public bool nightMode;


	public Vector3 pointTweenPosition;
	public Vector3 objTweenPosition;
	public float turnTimer = 10;
	public float points;
	public float Level1Points= 500;
	public float Level3Points = 1250;
	public float Level4Points = 2000;
	public bool inMenus;
	public bool passiveWorld = true;
	public bool fading = false;
	//SAVES AND USES TO FIND NON-ACTIVE FOOD
	private GameObject[] foodList;
	private GameObject bG;
	private Vector2 bGVel = new Vector2(0,0);
	
	public Vector3 moveDirection;
	
	void Awake()
	{

		if(instance !=null)
		{
			GameObject.Destroy(instance);
		}
		else
		{
			instance = this;
		}
		Application.targetFrameRate = 30;

		DontDestroyOnLoad(this);
		
		iTween.CameraFadeAdd();
		
		bG = GameObject.Find("BG");
		FC = Food.GetComponent<FoodController>();
		getMoveDirection();
		inMenus = true;

		if(!PlayerPrefs.HasKey("DayNightMode"))
		{
			PlayerPrefs.SetInt("DayNightMode",0);
		}
		
	}
	void Update()
	{
		
		if(inMenus)
		{

			return;
		}

		turnTimer -= Time.deltaTime;
		moveTimer -= Time.deltaTime;

		/*
		if(moveTimer< 0)
		{
			getMoveDirection();
			moveTimer =3;
		}
		GameObject.Find("Background").transform.position += moveDirection/25;
		*/
		
		#region 10 SECOND WORLD TRANSITION
		if(turnTimer < 0)
		{
			turnTimer = 10;
			
			if(player)
			{
				GameObject.Find("GM").GetComponent<GameManager>().SpawnEnemys();
				GameObject.Find("GM").GetComponent<GameManager>().SpawnFood();

				if(freePlay)
				{
					Globals.instance.player.GetComponent<PlayerController>().invincibleUsed = false;
					Globals.instance.player.GetComponent<PlayerController>().upgradedSpeed = false;
					Globals.instance.player.GetComponent<PlayerController>().upgradedGravity = false;
					Globals.instance.player.transform.FindChild("GravityUpgradeEffect").collider.enabled = false;
					Globals.instance.player.GetComponentInChildren<ParticleSystem>().Stop();
					Globals.instance.player.GetComponentInChildren<ParticleSystem>().Clear();
					GameObject.Find("SoundFX").audio.Stop();

					return;
				}

				GameObject.Find("SoundFX").audio.Stop();
				
				passiveWorld = !passiveWorld;
				Globals.instance.player.GetComponent<PlayerController>().invincibleUsed = false;
				Globals.instance.player.GetComponent<PlayerController>().upgradedSpeed = false;
				Globals.instance.player.GetComponent<PlayerController>().upgradedGravity = false;
				Globals.instance.player.transform.FindChild("GravityUpgradeEffect").collider.enabled = false;
				Globals.instance.player.GetComponentInChildren<ParticleSystem>().Stop();
				Globals.instance.player.GetComponentInChildren<ParticleSystem>().Clear();
				Globals.instance.player.GetComponent<PlayerController>().moveSpeed = 1500;
				
				#region Passive World
				if(passiveWorld)
				{
					
					
					
					
				RenderSettings.fog = false;
//				Globals.instance.player.GetComponent<Light>().intensity = 8;
				foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
				{
					if(go.gameObject.GetComponent<KillerController>())
					{
						go.transform.localScale/=3;
							
					}
					else if(go.gameObject.GetComponent<TweenerController>())
						{
							go.gameObject.GetComponent<TweenerController>().TweenSmall();
						}
				}
					//grabs all the food and shows it
					foreach(GameObject food in foodList)
					{
						if(food)
						{
						food.collider.enabled = true;
						food.GetComponentInChildren<MeshRenderer>().enabled = true;
						food.GetComponentInChildren<ParticleSystem>().renderer.enabled = true;
						}
						
					}
				}
				#endregion
				
				#region Aggressive World
				else
				{
					RenderSettings.fog = true;
					
					Globals.instance.player.GetComponent<Light>().intensity = 1;
					
					foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
					{
						//go.transform.FindChild("Spotlight").GetComponent<Light>().intensity = 1;
						if(go.gameObject.GetComponent<KillerController>())
						{
							go.transform.localScale*=3;
							
					
						}
						else if(go.gameObject.GetComponent<TweenerController>())
						{
							go.gameObject.GetComponent<TweenerController>().TweenBig();
						}
					}
					
//					//set BG material to night material
//					foreach(GameObject go in GameObject.FindGameObjectsWithTag("BG"))
//					{
//						go.renderer.material = Resources.Load("Materials/BG_02") as Material;
//					}
					
					//grabs 70% of all the food and hides it
					foodList = GameObject.FindGameObjectsWithTag("Food");
					foreach(GameObject food in foodList)
					{
						int r = Random.Range(1, 101);
						if(r <= 70)
						{
							//food.SetActive(false);
							food.collider.enabled = false;
							food.GetComponentInChildren<MeshRenderer>().enabled = false;
							food.GetComponentInChildren<ParticleSystem>().renderer.enabled = false;
							
						}
					}
				}
				#endregion
			}
		}
		#endregion
		
		if(turnTimer<0.5f && !freePlay)
		{
			
			if(!fading)
			{
				StartCoroutine(FadeWorld(1));
				
				
				
			}
		}
	}
	
	IEnumerator FadeWorld(float tweenTime)
//	public void FadeWorld()
	{
		fading = true;
		
		if(passiveWorld)
		{
			//fades playermovent sound to lower pitch
			GameObject.Find("SoundFXFade").audio.PlayOneShot(Resources.Load("Sounds/fade_01") as AudioClip);
			iTween.AudioTo(GameObject.Find("SoundFXFade"), iTween.Hash(
				"pitch", 0.5f,
				"time", tweenTime));
			
			//fades all the BG's to a red color
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("BG"))
			{
				Debug.Log("tween bg's");

				iTween.ColorTo(go, iTween.Hash(
					"r", 1,
					"g", 0.08f,
					"b", 0.08f,
					"a", 1,
					"time", tweenTime));

			}
		}
		else if(!passiveWorld)
		{
			//fades playermovent sound to normal pitch
			GameObject.Find("SoundFXFade").audio.PlayOneShot(Resources.Load("Sounds/fade_01") as AudioClip);
			iTween.AudioTo(GameObject.Find("SoundFXFade"), iTween.Hash(
				"pitch", 1,
				"time", tweenTime));
			
			
			
			//fades all the BG's back to normal MAIN color
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("BG"))
			{

				iTween.ColorTo(go, iTween.Hash(
					"r", 1,
					"g", 1,
					"b", 1,
					"a", 1,
					"time", tweenTime));

			}
		}
		
		yield return new WaitForSeconds(tweenTime - (tweenTime - 0.7f));
		GameObject.Find("SoundFXFade").audio.PlayOneShot(Resources.Load("Sounds/fade_01") as AudioClip);
		yield return new WaitForSeconds(tweenTime - 0.7f);
		DoneFading(tweenTime);
		
		//fades Camera to BLACK
//		iTween.CameraFadeTo(iTween.Hash(
//				"amount" , 0.5f,
//				"time" , 0.10f,
//				"oncomplete", "DoneFading",
//				"oncompletetarget", gameObject));
		
	}
	
	public void DoneFading(float tweenTime)
	{
		//Debug.Log("ITS CALLED");
		fading = false;

		//fades Camera from BLACK, to the appropriate "darkness" depending on current world state
//		if(passiveWorld)
//		{
//		iTween.CameraFadeTo(iTween.Hash(
//				"amount" , 0.0f,
//				"time" , 0.10f));
//			
//		}
//		else
//		{iTween.CameraFadeTo(iTween.Hash(
//				"amount" , 0.2f,
//				"time" , 0.25f));
//			
//		}
	}
	public void LoadPlayPanel()
	{
		playPanel.SetActive(true);
		titleMenuPanel.SetActive(false);
	}
	
	public void LoadTitlePanel()
	{
		playPanel.SetActive(false);
		titleMenuPanel.SetActive(true);
	}
	
	public void getMoveDirection()
	{	
		moveDirection = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),0);
	}
}
