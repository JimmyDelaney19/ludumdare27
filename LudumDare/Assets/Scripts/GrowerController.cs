using UnityEngine;
using System.Collections;

public class GrowerController : Controller {
	
	public GameObject closestFood;
	public float moveSpeed = 100000;
	public GameObject particles;
	public ParticleSystem playerDeath;
	public float growthRate = 8;
	
	// Use this for initialization
	void Start () 
	{
		GetClosestFood();
	}
	
	// Update is called once per frame
	void Update () 
	{		
		if(!closestFood)
		{
			GetClosestFood();
		}
		
		transform.up = Vector3.Lerp(transform.up, -rigidbody.velocity, Time.deltaTime);
		
		if(Globals.instance.passiveWorld)
		{
			MoveToFood();
		}
		else
		{
			AttackPlayer();
		}
	}
	
	void AttackPlayer()
	{
		if(Globals.instance.player)
		{
			if(Vector3.Distance(Globals.instance.player.transform.position,transform.position)<35)
			{
				rigidbody.AddForce((Globals.instance.player.transform.position - transform.position).normalized * Time.deltaTime * moveSpeed);
			}
		}
	}
	
	void MoveToFood()
	{
		if(closestFood)
		{
			rigidbody.AddForce((closestFood.transform.position - transform.position).normalized * Time.deltaTime * moveSpeed);
		}
	}
	
	void GetClosestFood()
	{
		if(Globals.instance.passiveWorld)
		{
		
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Food"))
			{
				
				if(!closestFood)
				{
					closestFood = go;
				}
				else
				{
					if(Vector3.Distance(go.transform.position,transform.position) < Vector3.Distance(closestFood.transform.position,transform.position))
					{
						closestFood = go;
					}
				}
			}
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Controller>())
		{
			if(other.GetComponent<PlayerController>())
			{
				if(Globals.instance.passiveWorld)//if its a passive world, then bigger eats smaller
				{
				
					if(other.gameObject.transform.localScale.x > transform.localScale.x)
					{
						//Cell kill sound; when food gets eaten
						SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.FoodEat);
						
						other.transform.localScale += transform.localScale/growthRate;
						Globals.instance.points +=transform.localScale.x*100;
						Globals.instance.pointLabel.GetComponent<UILabel>().text = (transform.localScale.x*100f).ToString("00");
						TweenPosition.Begin(Globals.instance.pointLabel,0,new Vector3(0,0,0));
						GameManager.instance.TweenPoints();
						GameObject.Find("GUI").GetComponent<GUIScript>().UpdateScore();
						Destroy(gameObject);
						
					}
					else
					{
						transform.localScale += other.transform.localScale/growthRate;
						particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
						SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.PlayerDeath);
						
						if(Globals.instance.player)
						{
						//particle.transform
						//particles.transform.localScale = Globals.instance.player.transform.localScale;
						}
						Destroy(other.gameObject);
					}
					
				}
				else //if its aggressive it doesn't matter how big you are
				{
					transform.localScale += other.transform.localScale/growthRate;
					particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
					SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.PlayerDeath);
					
					if(Globals.instance.player)
					//particles.transform.localScale = Globals.instance.player.transform.localScale;
					Destroy(other.gameObject);
				}
				
			}
			else//its enemy on enemy action
			{
				if(other.gameObject.transform.localScale.x > transform.localScale.x && !other.GetComponent<KillerController>())
				{
					other.transform.localScale += transform.localScale/growthRate;
					Destroy(gameObject);
					
				}
				else if(other.GetComponent<KillerController>())
				{
					Destroy(gameObject);
				}
				else
				{
					transform.localScale += other.transform.localScale/growthRate;
					Destroy(other.gameObject);
				}
			}
		} 
	}
	
}
