﻿using UnityEngine;
using System.Collections;

public class HugeController : Controller {
	
	public GameObject closestEnemy;
	public float moveSpeed = 100;
	public GameObject particles;
	public ParticleSystem playerDeath;
	public Animator anim;
	public float growthRate = 100;
	// Use this for initialization
	void Start () {
		
		GetClosestEnemy();
		anim = transform.FindChild("Bemoth").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!closestEnemy)
		{
			GetClosestEnemy();
		}
		
		transform.up = Vector3.Lerp(transform.up, -rigidbody.velocity, Time.deltaTime);

		if(closestEnemy && Vector3.Distance(closestEnemy.transform.position,transform.position) < 10 && Globals.instance.passiveWorld)
		{
			//Debug.Log(transform.FindChild("Bemoth").GetComponent<Animator>().GetCurrentAnimationClipState(0));
			//anim.CrossFade("Bemoth Chasing State",0.1f);
			anim.Play("Bemoth Chasing State");
			moveSpeed = 550;
		}
		else
		{
			moveSpeed = 150;
			anim.Play("Bemoth Idle State");
		}
		if(Globals.instance.passiveWorld)
		{

			if(closestEnemy)
				MoveToEnemy();
		}
		else
		{

		}
	}
	
	void MoveToEnemy()
	{
		if(closestEnemy)
		{
			rigidbody.AddForce((closestEnemy.transform.position - transform.position).normalized * Time.deltaTime * moveSpeed);
		}
	}
	
	void GetClosestEnemy()
	{
		if(Globals.instance.passiveWorld)
		{
		
			foreach(GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				
				if(closestEnemy)
				{
					if(Vector3.Distance(go.transform.position,transform.position) < Vector3.Distance(closestEnemy.transform.position,transform.position) && go.transform.localScale.x < transform.localScale.x && !go.GetComponent<HugeController>()&& !go.GetComponent<KillerController>())
					{
						if(gameObject!=go)
						closestEnemy = go;
					}
				}
				else if(go.transform.localScale.x > transform.localScale.x -1.5f)
				{
					if(gameObject!=go)
					closestEnemy = go;
				}
			}
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Controller>())
		{
			if(other.GetComponent<PlayerController>())
			{
				if(Globals.instance.passiveWorld)//if its a passive world, then bigger eats smaller
				{
				
					if(other.gameObject.transform.localScale.x > transform.localScale.x)
					{
						other.transform.localScale += transform.localScale/10;
						Globals.instance.points +=transform.localScale.x*100;
						
					Globals.instance.pointLabel.GetComponent<UILabel>().text = (transform.localScale.x*100f).ToString("00");
						TweenPosition.Begin(Globals.instance.pointLabel,0,new Vector3(0,0,0));
						GameManager.instance.TweenPoints();
						GameObject.Find("GUI").GetComponent<GUIScript>().UpdateScore();
						Destroy(gameObject);
						
						
					}
					else
					{
						//transform.localScale += other.transform.localScale/10;
						particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
						SM.instance.PlayOneShot (SM.instance.SoundFX, SM.instance.PlayerDeath);
						
						Destroy(other.gameObject);
					}
					
				}
				else //if its aggressive it doesn't matter how big you are
				{
					transform.localScale += other.transform.localScale/10;
					particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
					SM.instance.PlayOneShot (SM.instance.SoundFX, SM.instance.PlayerDeath);
					
					if(Globals.instance.player)
						//particles.transform.localScale = Globals.instance.player.transform.localScale;
						Destroy(other.gameObject);
				}
				
			}
			else
			{
				if(other.gameObject.transform.localScale.x > transform.localScale.x && !other.GetComponent<KillerController>())
				{
					//other.transform.localScale += transform.localScale/growthRate;
					//Destroy(gameObject);
					
				}
				else if(other.GetComponent<KillerController>())
				{
					Destroy(gameObject);
				}
				else
				{
					float tempFloat = (transform.localScale.x/growthRate)/(Mathf.Pow(transform.localScale.x,0.25f));

					transform.localScale += new Vector3(tempFloat, tempFloat, tempFloat);
					Destroy(other.gameObject);
				}
			}
		} 
	}
}
