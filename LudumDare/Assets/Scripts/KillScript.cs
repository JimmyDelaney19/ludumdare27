﻿using UnityEngine;
using System.Collections;

public class KillScript : MonoBehaviour {
	public float lifeTime = 0.5f;
	public string deathMessage;
	// Use this for initialization
	void Start () {

	if(gameObject.name == "PlayerDeath(Clone)")
			{
			int random = Random.Range(0,12); //Note: random concatenates down to its nearest whole number
			if(random ==0)
			{
				deathMessage = "Celly fell flat on her face";
			}
			else if(random ==1)
			{
				deathMessage = "Celly went to sleep with the fishes";
			}
			else if(random ==2)
			{
				deathMessage = "Celly got tired :(";
			}
			else if(random ==3)
			{
				deathMessage = "That could have gone better...";
			}
			else if(random ==4)
			{
				deathMessage = "Celly turned into fertilizer";
			}
			else if(random ==5)
			{
				deathMessage = "And just like that, Celly vanished";
			}
			else if(random ==6)
			{
				deathMessage = "Celly just wanted a hug :(";
			}
			else if(random ==7)
			{
				deathMessage = "Oops!";
			}
			else if(random ==8)
			{
				deathMessage = "Celly's pushing up daisies";
			}
			else if(random ==9)
			{
				deathMessage = "Celly's now cell food...";
			}
			else if(random ==10)
			{
				deathMessage = "You've been absorbed...";
			}
			else if(random ==11)
			{
				deathMessage = "Well then...";
			}
			
			/*
			if(random ==0)
			{
				deathMessage = "Celly fell flat on her face";
			}
			else if(random ==1)
			{
				deathMessage = "Celly went to sleep with the fishes";
			}
			else if(random ==2)
			{
				deathMessage = "Celly got tired :(";
			}
			else if(random ==3)
			{
				deathMessage = "That could have gone better...";
			}
			else if(random ==4)
			{
				deathMessage = "E for Effort!";
			}
			else if(random ==5)
			{
				deathMessage = "Celly turned into fertilizer";
			}
			else if(random ==6)
			{
				deathMessage = "And just like that, Celly vanished";
			}
			else if(random ==7)
			{
				deathMessage = "Celly dove headfirst into a blender";
			}
			else if(random ==8)
			{
				deathMessage = "Celly found a new meaning to life";
			}
			else if(random ==9)
			{
				deathMessage = "Are you even trying?";
			}
			else if(random ==10)
			{
				deathMessage = "Celly just wanted a hug :(";
			}
			else if(random ==11)
			{
				deathMessage = "Let someone else try...";
			}
			else if(random ==12)
			{
				deathMessage = "I bet you won't play again...";
			}
			else if(random ==13)
			{
				deathMessage = "What happened to those skills?";
			}
			else if(random ==14)
			{
				deathMessage = "Oops!";
			}
			else if(random ==15)
			{
				deathMessage = "Celly's pushing up daisies";
			}
			else if(random ==16)
			{
				deathMessage = "That's not how you do it...";
			}
			else if(random ==17)
			{
				deathMessage = "Celly's now cell food...";
			}
			else if(random ==18)
			{
				deathMessage = "You've been absorbed...";
			}
			else if(random ==19)
			{
				deathMessage = "Well then...";
			}
			*/
			
				//GameObject.Find("Music").audio.
			iTween.AudioTo(GameObject.Find("Music"),0,1,2);
			iTween.AudioTo(GameObject.Find("SoundFX"),0,1,2);
			float fp = GameObject.Find("SoundFXPlayer").audio.pitch;
			iTween.AudioTo(GameObject.Find("SoundFXPlayer"),0,fp,2);
			/*
			Globals.instance.deathLabel.SetActive(true);
			Globals.instance.deathLabel.GetComponent<UILabel>().text = deathMessage;
			*/
			Globals.instance.pointLabel.GetComponent<UILabel>().text = "";
			}
		
	}
	
	// Update is called once per frame
	void Update () {
	lifeTime -= Time.deltaTime;
		
	if(lifeTime < 0)
		{
			if(gameObject.name == "PlayerDeath(Clone)")
			{
				SM.instance.SoundFX.audio.Stop ();
				Globals.instance.deathLabel.SetActive(false);
				GameObject.Find("GUI").GetComponent<GUIScript>().ShowLevel();
			}
			Destroy(gameObject);
			
		}
	}
}
