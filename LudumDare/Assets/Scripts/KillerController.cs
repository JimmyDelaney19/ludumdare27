﻿using UnityEngine;
using System.Collections;

public class KillerController : Controller {
	
	public GameObject closestFood;
	public float moveSpeed = 500;
	public ParticleSystem playerDeath;
	public Vector3 tarPos;
	public GameObject particles;
	public GameObject killerParticles;
	public float moveTimer = 2f;
	// Use this for initialization
	void Start () 
	{
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		moveTimer-=Time.deltaTime;
		transform.up = Vector3.Lerp(transform.up, -rigidbody.velocity, Time.deltaTime);	
		if(moveTimer<0)
		{		tarPos = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),0).normalized;
				moveKiller ();
				moveTimer = 2f;
		}
		moveKiller();
	}
	
	public void moveKiller()
	{
		rigidbody.AddForce(tarPos * Time.deltaTime * moveSpeed);
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<Controller>())
		{
			if(other.GetComponent<PlayerController>())
			{
				
						
						particles = Instantiate(playerDeath,Globals.instance.player.transform.position,transform.rotation) as GameObject;
						SM.instance.PlayOneShot(SM.instance.SoundFX, SM.instance.PlayerDeath);
						Destroy(other.gameObject);
				
				
			}
			else
			{
					particles = Instantiate(killerParticles,other.transform.position,transform.rotation) as GameObject;
					particles.transform.localScale = other.transform.localScale;
					Destroy(other.gameObject);

			}
		} 
	}
}
