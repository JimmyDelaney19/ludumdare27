﻿using UnityEngine;
using System.Collections;

public class PlayerController : Controller {
	
	//MOVEMENT
	public float moveSpeed = 2500;
	public int mouseRange;
	private float moveTime;
	public float movementSoundSwellRate = 0.25f;
	public float soundVolume = 0.3f;
	public float currentFrom = 1.0f;
	public float currentTo = 1.05f;

	public bool upgradedSpeed;
	public bool upgradedSpeedSwitch; //used to lock in the speed variable
	public bool upgradedGravity;
	public bool invincible;
	public bool invincibleUsed;
	public float invincibleTimer = 1.5f;
	
	//EFFECTS
	private ParticleSystem ripples;
	
	// Use this for initialization
	void Start () 
	{
		ripples = transform.FindChild("Player Ripples").particleSystem  as ParticleSystem;
		
		rigidbody.AddForce(100,0,0);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log(rigidbody.velocity);
//		if(GameObject.Find("GM").GetComponent<GameManager>().gameStarted == false) return;
		transform.up = Vector3.Lerp(transform.up, -rigidbody.velocity, Time.deltaTime);
		moveInput();
		SpecialInput();
		
		for (int i = 0; i < Globals.instance.BG1.Length; i++)
		{
			//Globals.instance.BG1[i].renderer.material.SetTextureOffset("_MainTex", new Vector2(-rigidbody.velocity.x/600 + 0, -rigidbody.velocity.y/600 + 0));
			//Globals.instance.BG2[i].renderer.material.SetTextureOffset("_MainTex", new Vector2(-rigidbody.velocity.x/400 + 1, -rigidbody.velocity.y/400 + 1));
			//Globals.instance.BG3[i].renderer.material.SetTextureOffset("_MainTex", new Vector2(-rigidbody.velocity.x/300 + 2, -rigidbody.velocity.y/300 + 2));
			//Globals.instance.BG4[i].renderer.material.SetTextureOffset("_MainTex", new Vector2(-rigidbody.velocity.x/300 + 3, -rigidbody.velocity.y/300 + 3));
			
			
			Globals.instance.BG1[i].renderer.material.mainTextureOffset += new Vector2(-rigidbody.velocity.x/10000,  -rigidbody.velocity.y/10000);
			Globals.instance.BG2[i].renderer.material.mainTextureOffset += new Vector2(-rigidbody.velocity.x/5000,   -rigidbody.velocity.y/5000);
			Globals.instance.BG3[i].renderer.material.mainTextureOffset += new Vector2(-rigidbody.velocity.x/3000,   -rigidbody.velocity.y/3000);
			Globals.instance.BG4[i].renderer.material.mainTextureOffset += new Vector2(-rigidbody.velocity.x/2500,   -rigidbody.velocity.y/2000);
			
			
		}
		
		//Circle Ripple Spawn Rate Effect
		ripples.emissionRate = (rigidbody.velocity.magnitude * .75f);
		//Debug.Log ("Ripple Emission Rate = "+ripples.emissionRate+".");
		ripples.startRotation = (transform.localEulerAngles.z * (3.14159265359f / -180));
		//Debug.Log ("transform.rotation.z = "+transform.rotation.z);
		Debug.Log ("StartRotation = "+ripples.startRotation+".");
		//Debug.Log ("StartRotation's source = "+transform.name+".");
		
		#region SOUND CONTROL
		SM.instance.SoundFXPlayer.audio.volume = (rigidbody.velocity.magnitude * .005f);
		
		if(upgradedSpeed && !upgradedSpeedSwitch)
		{
			TweenUpgradedSpeed();
		}
		else if(!upgradedSpeed && upgradedSpeedSwitch)
		{
			upgradedSpeedSwitch = false; //switch OFF
			
			iTween.AudioTo(SM.instance.SoundFXPlayer, iTween.Hash(
				"pitch", 0.4f,
				"time", 1));
		}
		#endregion
		
		if(invincible)
		{
			invincibleTimer -= Time.deltaTime;
		}
		
		if(invincibleTimer < 0 )
		{
			invincible = false;
			collider.enabled = true;
			invincibleTimer = 1.5f;
		}
	}
	public void SpecialInput()
	{
		if(Input.GetKeyDown(KeyCode.A) && !invincible && !invincibleUsed)
		{
			/*
			GetComponent<ParticleSystem>().Play ();
			invincible = true;
			collider.enabled = false;
			invincibleUsed = true;
			*/
//			GameObject.Find("GM").GetComponent<GameManager>().RestartLevel();
		}
	}
	
	public void moveInput()
	{
		if(Input.anyKey || Input.acceleration.x != 0 || Input.acceleration.y != 0)
		{
			
			#region Phone Tilt
			/*
			if(Input.acceleration.x != 0 || Input.acceleration.y != 0)
			{
				Debug.Log("Orientation = "+Input.deviceOrientation+"; Accel = "+Input.acceleration);
				
				//LandscapeLeft
				//Base	0.00,-1.00, 0.00
				//	Nu  0.00,-0.70,-0.70
				//	Up  0.00,-0.45,-0.95
				//	Dn  0.00,-0.95,-0.45
				//	Rt  0.00,-0.70,-0.70
				//	Lt -0.25,-0.70,-0.70
				
				//THIS IS THE INPUT FOR ANDROID DEVICES
				if(Input.acceleration.x < -0.1f) //LEFT
				{
					rigidbody.AddForce( new Vector3(-1,0,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.acceleration.y < -0.8f) //DOWN
				{
					rigidbody.AddForce( new Vector3(0,-1,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.acceleration.x > 0.1f) //RIGHT
				{
					rigidbody.AddForce( new Vector3(1,0,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.acceleration.y > -0.6f) //UP
				{
					rigidbody.AddForce( new Vector3(0,1,0).normalized*Time.deltaTime*moveSpeed);
				}
				
				
				//Portrait
				//Base	1.00, 0.00, 0.00
				//	Nu  0.70, 0.00,-0.70
				//	Up  0.45, 0.00,-0.95
				//	Dn  0.95, 0.00,-0.45
				//	Rt  0.70, 0.25,-0.70
				//	Lt  0.70,-0.25,-0.70
				
				//THIS IS THE INPUT FOR ANDROID DEVICES
	//			if(Input.acceleration.y < -0.1f) //LEFT
	//			{
	//				rigidbody.AddForce( new Vector3(-1,0,0).normalized*Time.deltaTime*moveSpeed);
	//			}
	//			if(Input.acceleration.x > 0.8f) //DOWN
	//			{
	//				rigidbody.AddForce( new Vector3(0,-1,0).normalized*Time.deltaTime*moveSpeed);
	//			}
	//			if(Input.acceleration.y > 0.1f) //RIGHT
	//			{
	//				rigidbody.AddForce( new Vector3(1,0,0).normalized*Time.deltaTime*moveSpeed);
	//			}
	//			if(Input.acceleration.x < 0.6f) //UP
	//			{
	//				rigidbody.AddForce( new Vector3(0,1,0).normalized*Time.deltaTime*moveSpeed);
	//			}
			}
			*/
			#endregion
			
			#region Mouse Control
			if(Input.GetMouseButton(0))
			{
				
				
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				if(Physics.Raycast(ray, out hit))
				{
					float dist = Vector3.Distance(transform.position, hit.point);
					float distPercent;
					if(dist > mouseRange)
					{
						dist = mouseRange;
					}
					distPercent = dist/mouseRange;
					rigidbody.AddForce( (hit.point-transform.position).normalized * (distPercent * moveSpeed) * Time.deltaTime);
				}
			}
			#endregion
			
			#region Arrow Key Control
			else
			{
				
				
				if(Input.GetKey(KeyCode.UpArrow))
				{
					rigidbody.AddForce( new Vector3(0,1,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.GetKey(KeyCode.LeftArrow))
				{
					rigidbody.AddForce( new Vector3(-1,0,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.GetKey(KeyCode.DownArrow))
				{
					rigidbody.AddForce( new Vector3(0,-1,0).normalized*Time.deltaTime*moveSpeed);
				}
				if(Input.GetKey(KeyCode.RightArrow))
				{
					rigidbody.AddForce( new Vector3(1,0,0).normalized*Time.deltaTime*moveSpeed);
				}
			}
			#endregion
			
		}
	}
	
	public void TweenUpgradedSpeed()
	{
		//Natural pitch for Player Sound is 0.4
		if(upgradedSpeed)
		{
			upgradedSpeedSwitch = true; //switch ON
			
			float f = (SM.instance.SoundFXPlayer.audio.pitch + 0.2f);
			iTween.AudioTo(SM.instance.SoundFXPlayer, iTween.Hash(
				"pitch", f,
				"time", 1));
		}
		
		
	}

	public void TurnOffSpeed()
	{
		Invoke("TurnOffSpeedDelayed",5);
	}

	public void TurnOffSpeedDelayed()
	{
		
		Globals.instance.player.GetComponent<PlayerController>().upgradedSpeed = false;
		Globals.instance.player.GetComponent<PlayerController>().moveSpeed = 1500;
		SM.instance.SoundFX.audio.Stop ();
	}

	public void TweenScale(float from, float to)
	{
	
		iTween.Stop(gameObject);
		transform.localScale = new Vector3(currentFrom,currentFrom,currentFrom);
		//Debug.LogError("FROM: " + from);
		//Debug.LogError("TO: " + to);
		currentFrom = from;
		currentTo = to;
		iTween.ValueTo(gameObject, iTween.Hash(
			"from" , from,
			"to"	, from*1.25,
			"easetype" , iTween.EaseType.easeOutQuad,
			"onupdate" , "ChangeScale",
			"onupdatetarget" , gameObject,
			"speed" , 1.0f,
			"oncomplete" , "ShrinkBack",
			"oncompletetarget" , gameObject)); 
	}

	public void ShrinkBack()
	{
		iTween.ValueTo(gameObject, iTween.Hash(
			"from" , currentFrom*1.25,
			"to"	, currentTo,
			"easetype" , iTween.EaseType.easeOutQuad,
			"onupdate" , "ChangeScale",
			"onupdatetarget" , gameObject,
			"speed" , 1.0f)); 
	}

	void ChangeScale(float value)
	{
		transform.localScale = new Vector3(value, value, value);
	}

	
}
