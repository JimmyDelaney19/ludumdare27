﻿using UnityEngine;
using System.Collections;

public class PowerUpGravityController : MonoBehaviour {
	
	//public GameObject PowerUpSpeed;
	public float moveSpeed = 10;
	public float moveTimer = 0f;
	public Vector3 tarPos;
	public ParticleSystem upgradeParticles;
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	moveTimer-=Time.deltaTime;
		
	if(moveTimer<0)
	{		tarPos = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),0).normalized;
			moveFood ();
			moveTimer = 0.3f;
	}
		moveFood();
	}
	
	public void moveFood()
	{
		rigidbody.AddForce(tarPos * Time.deltaTime * moveSpeed);
	}
	
	void OnTriggerEnter(Collider other)
	{
		//if the thing the food hits is an enemy or player
		if(other.GetComponent<PlayerController>()&& !other.GetComponent<PlayerController>().upgradedGravity)
		{
			other.GetComponentInChildren<ParticleSystem>().Play();
			other.GetComponent<PlayerController>().upgradedGravity = true;
			Globals.instance.player.transform.FindChild("GravityUpgradeEffect").GetComponent<SphereCollider>().enabled = true;
			//Globals.instance.player.transform.GetComponentInChildren<ParticleSystem>().

			//Instantiate(upgradeParticles,transform.position,transform.rotation);
			SM.instance.SoundFX.audio.clip = SM.instance.FoodPowerup02; //magnet
			SM.instance.SoundFX.audio.Play();
			if(Globals.instance.freePlay)
			{
				Globals.instance.turnTimer = 5;
			}
			Destroy(gameObject);
			
			
		}
	}
}
