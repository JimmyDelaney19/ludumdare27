﻿using UnityEngine;
using System.Collections;

public class PowerUpSpeedController : MonoBehaviour {
	
	public GameObject PowerUpSpeed;
	public float moveSpeed = 10;
	public float moveTimer = 0f;
	public Vector3 tarPos;
	public ParticleSystem upgradeParticles;
	// Use this for initialization
	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	moveTimer-=Time.deltaTime;
		
	if(moveTimer<0)
	{		tarPos = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),0).normalized;
			moveFood ();
			moveTimer = 0.3f;
	}
		moveFood();
	}
	
	public void moveFood()
	{
		rigidbody.AddForce(tarPos * Time.deltaTime * moveSpeed);
	}
	
	void OnTriggerEnter(Collider other)
	{
		//if the thing the food hits is an enemy or player
		if(other.GetComponent<PlayerController>()&& !other.GetComponent<PlayerController>().upgradedSpeed)
		{
			other.GetComponent<PlayerController>().moveSpeed *=1.5f;
			other.GetComponent<PlayerController>().upgradedSpeed = true;
			Globals.instance.player.GetComponent<PlayerController>().TurnOffSpeed();
			//Spawn and set speed particle to player & Play Sound
			GameObject p = Instantiate(upgradeParticles,transform.position,transform.rotation) as GameObject;
//			p.transform.localScale = GameObject.Find("Player").transform.localScale;
//			p.particleSystem.loop = true;
//			p.transform.parent = GameObject.Find("Player").transform;
			//Globals.instance.tu
			SM.instance.SoundFX.audio.clip = SM.instance.FoodPowerup01;
			SM.instance.SoundFX.audio.Play ();

			if(Globals.instance.freePlay)
			{
				Globals.instance.turnTimer = 5;
			}
			Destroy(gameObject);
			
			
		}
	}


}
