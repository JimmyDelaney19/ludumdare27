﻿using UnityEngine;
using System.Collections;

public class SM : MonoBehaviour
{
		public static SM instance;
		
		public GameObject Music;
		public GameObject SoundFX;
		public GameObject SoundFXFade;
		public GameObject SoundFXPlayer;
		
		public AudioClip Button01;
		public AudioClip Button02;
		public AudioClip Button03;
		public AudioClip Button04;
		public AudioClip CreatureBemoth01;
		public AudioClip CreatureBemoth02;
		public AudioClip CreatureBemoth03;
		public AudioClip CreatureGorgi01;
		public AudioClip CreatureGorgi02;
		public AudioClip CreatureGorgi03;
		public AudioClip CreatureHidybite01;
		public AudioClip CreatureHidybite02;
		public AudioClip CreatureHidybite03;
		public AudioClip CreatureJellybean01;
		public AudioClip CreatureJellybean02;
		public AudioClip CreatureJellybean03;
		public AudioClip CreatureMemakey01;
		public AudioClip CreatureMemakey02;
		public AudioClip CreatureMemakey03;
		public AudioClip CreatureNotuchi01;
		public AudioClip CreatureNotuchi02;
		public AudioClip CreatureNotuchi03;
		public AudioClip CreatureSmorgi01;
		public AudioClip CreatureSmorgi02;
		public AudioClip CreatureSmorgi03;
		public AudioClip FoodDrop;
		public AudioClip FoodEat;
		public AudioClip FoodPowerup01;
		public AudioClip FoodPowerup02;
		public AudioClip FoodPowerup03;
		public AudioClip MusicMenu01;
		public AudioClip MusicMenu02;
		public AudioClip MusicInGame01;
		public AudioClip MusicInGame02;
		public AudioClip MusicInGame03;
		public AudioClip PlayerMovement;
		public AudioClip PlayerDeath;
		
		
		void Awake ()
		{
				instance = this;
		}
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void PlayOneShot (GameObject go, AudioClip ac)
		{
				if (go != null) {
						go.audio.PlayOneShot (ac);
				} else {
						go = GameObject.Find("SoundFX") as GameObject;
						go.audio.PlayOneShot (ac);
				}
		}
}
