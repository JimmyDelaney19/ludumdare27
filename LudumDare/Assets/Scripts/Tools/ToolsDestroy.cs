﻿using UnityEngine;
using System.Collections;

public class ToolsDestroy : MonoBehaviour 
{
	
	public float TimeTillDestroy;
	
	void Start()
	{
		
	}
	
	void Update()
	{
		//Every frame, subtracts (the time it took to call this frame from the last one) from the Timer
		TimeTillDestroy -= Time.deltaTime;
		
		if(TimeTillDestroy < 0)
		{
			//destroys this gameObject and everything parented to it
			Destroy(gameObject);
		}
		
	}
}
