﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]
public class ToolsTransformation : MonoBehaviour 
{
	public bool Acceleration, Impulse, VelocityChange;
	public Vector3 Force, RelativeForce, Torque, RelativeTorque;
	public Vector3 StaticRotation;
	private Vector3 v3;
	private ForceMode fm;
	
	void Start()
	{
		if(Acceleration)
		{
			fm = ForceMode.Acceleration;
//			Debug.Log("if Acceleration");
		}
		else if(Impulse)
		{
			fm = ForceMode.Impulse;
//			Debug.Log("if Impulse");
		}
		else if(VelocityChange)
		{
			fm = ForceMode.VelocityChange;
//			Debug.Log("if VelocityChange");
		}
		else
		{
			fm = ForceMode.Force;
//			Debug.Log("if Force");
		}
	}
	
	void Update()
	{
//		Debug.Log(fm);
		
		//physics rotation
		rigidbody.AddForce((Force * Time.deltaTime), fm);
		rigidbody.AddRelativeForce((RelativeForce * Time.deltaTime), fm);
		rigidbody.AddTorque((Torque * Time.deltaTime), fm);
		rigidbody.AddRelativeTorque((RelativeTorque * Time.deltaTime), fm);
		
		//static rotation
		transform.Rotate(StaticRotation, Space.Self);
		
	}
}
